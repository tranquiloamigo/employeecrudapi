// Model link
const Employee = require('../models/Employee')
// CustomException Helper
const customError = require('../helpers/exceptions')

// Get all employees from database
const employeeInstance_get = async (req, res) => {
    try {
        const EmployeeList = await Employee.find({})
        return res.status(200).json(EmployeeList)
    } catch(error) {
        return res.status(500).json(customError(500, 'Internal Server Error', 'Error encountered in the Server', error))
    }
}

// Create employee in database using POST
const employeeInstance_create = async (req, res) => {
    try {
        const EmployeeDoc = await Employee.create(req.body)
        return res.status(201).json(EmployeeDoc)
    } catch(error) {
        return res.status(500).json(error)
    }
}

// Update employee in database using PUT
const employeeInstance_update = async (req, res) => {
    try {
        // Find the employee record
        const EmployeeDoc = await Employee.findOneAndUpdate({_id: req.params.id}, req.body, {new: true})
        if (EmployeeDoc) {
            return res.status(200).json(EmployeeDoc)
        } else {
            return res.status(400).json(customError(400, 'Employee not found', 'Employee Document not found on database'))
        }
    } catch(error) {
        return res.status(500).json(customError(500, 'Internal Server Error', 'Error encountered in the Server', error))
    }
}

// Delete employee in database using DELETE
const employeeInstance_delete = async (req, res) => {
    try {
        const result = await Employee.findOneAndDelete({_id: req.params.id})
        console.log(result)
        if(result) {
            return res.status(200).json({success: true, message: 'Employee removed from database'})
        } else {
            return res.status(400).json(400, 'Employee not found', 'Employee Document not found on database')
        }    
    } catch (error) {
        return res.status(500).json(customError(500, 'Internal Server Error', 'Error encountered in the Server', error))
    }
}

module.exports = {
    employeeInstance_get,
    employeeInstance_create,
    employeeInstance_update,
    employeeInstance_delete
}