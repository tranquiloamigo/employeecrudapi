const express = require('express')
const router = express.Router()

// User controller module
const {userInstance_create} = require('../controllers/UserController')

router.post('/user', userInstance_create)

module.exports = router