const express = require('express')
const router = express.Router()

// Controller Modules
const employee_controller = require('../controllers/EmployeeController')

// GET request for employees
router.get('/employees', employee_controller.employeeInstance_get)

// POST request for creating an employee
router.post('/employees', employee_controller.employeeInstance_create)

// PUT request for updating an employee
router.put('/employees/:id', employee_controller.employeeInstance_update)

// DELETE request for deleting an employee
router.delete('/employees/:id', employee_controller.employeeInstance_delete)

module.exports = router