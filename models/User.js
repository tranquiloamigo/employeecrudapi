const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: [true, 'username is required']
    },
    password: {
        type: String,
        required: [true, 'password is required']
    }
})

userSchema.methods.verifyPasswordHash = async function(plaintextPassword) {
    return await bcrypt.compare(plaintextPassword, this.password)
}

module.exports = User = mongoose.model('User', userSchema)