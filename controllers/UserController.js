const User = require('../models/User')
const bcrypt = require('bcrypt')
const { Schema } = require('mongoose')
const customError = require('../helpers/exceptions')
const saltRounds = 10

// Register new user
const userInstance_create = async (req, res) => {
    // Check if password is present in the payload object
    if (!req.body.password){
        const exception = customError(400, 'Password required', 'password is empty')
        return res.status(400).json(exception)
    }
    try {
        req.body.password = await bcrypt.hash(req.body.password, saltRounds)
        const UserDoc = await User.create(req.body)
        return res.status(201).json(UserDoc)
    } catch(error) {
        // MongoDB error code 11000 == Value already exists in an indexed field
        if (error.code === 11000) {
            const exception = customError(409, 'username already taken', 'username already exists in the database', error)
            return res.status(409).json(exception)
        }
        return res.status(400).json(error)
    }
}

// Verify user and password
const userInstance_auth = async (req, res, next) => {
    // Check if both username and password is present in the payload object
    if (!req.body.username) {
        const exception = customError(400, 'username required', 'username is empty')
        return res.status(400).json(exception)
    }

    if (!req.body.password) {
        const exception = customError(400, 'password required', 'password is empty')
        return res.status(400).json(exception)
    }

    try {
        const UserDoc = await User.findOne({username: req.body.username})
        if (!UserDoc) {
            const exception = customError(401, 'Username does not exist', 'Username does not exist in database')
            return res.status(401).json(exception)
        }
        const boolean = await UserDoc.verifyPasswordHash(req.body.password)
        if (boolean) {
            next()
        } else {
            const exception = customError(401, 'Wrong Password', 'Invalid Password entered')
            return res.status(401).json(exception)
        }   
    } catch (error) {
        return res.status(500).json(customError(500, 'Internal Server Error', 'Internal Server Error'))
    }
}

module.exports = {
    userInstance_create,
    userInstance_auth
}