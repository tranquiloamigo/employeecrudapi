const mongoose = require('mongoose')

const employeeSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'firstname is required']
    },
    lastName: {
        type: String,
        required: [true, 'lastname is required']
    },
    email: {
        type: String,
        required: [true, 'email is required']
    },
    contact: {
        type: String,
        required: [true, 'contact number is required']
    },
    address: {
        type: String,
        required: [true, 'address is required']
    },
    dateEmployment: {
        type: Date,
        required: [true, 'Date of Employment is required']
    }
})

module.exports = Employee = mongoose.model('Employee', employeeSchema)