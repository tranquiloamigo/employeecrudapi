const express = require('express')
const router = express.Router()

// JWT controller module
const {supplyToken} = require('../controllers/JwtController')
const {userInstance_auth} = require('../controllers/UserController')

router.post('/auth', [userInstance_auth, supplyToken])

module.exports = router