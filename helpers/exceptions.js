// For custom exception handling

const customError = (code, message, description, details = {}) => {
    return {
        errors: [
            {
                code: code,
                message: message,
                description: description,
                details: details
            }
        ]
    }
}

module.exports = customError